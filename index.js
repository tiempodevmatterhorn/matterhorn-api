const { ApolloServer, gql } = require('apollo-server-express')
const express = require('express');
const app = express();

const passport = require('passport');
const config = require("./config")
var OIDCStrategy = require('passport-azure-ad').OIDCStrategy;

const { Client, Docket, Currency, UTBMS, User } = require("./components")


const baseTypeDef = gql` 
type Query
`

const server = new ApolloServer(
  {
    typeDefs: [baseTypeDef,
      Currency.typeDef,
      UTBMS.typeDef,
      Client.typeDef,
      User.typeDef,
      Docket.typeDef
    ],
    resolvers: [
      Currency.resolvers,
      UTBMS.resolvers,
      Client.resolvers,
      User.resolver,
      Docket.resolvers
    ],
    mocks: true,
    mockEntireSchema: false
  });

server.applyMiddleware({ app });
const normalizePort = (val) => {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
var port = normalizePort(process.env.PORT || '3000');

const listener = app.listen({ port }, () => {
  console.log(listener.address().address, ":", listener.address().port, server.graphqlPath)
  console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`)
})