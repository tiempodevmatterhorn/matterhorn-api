const { UTBMS } = require('./UTBMSHeroku')
const { typeDef } = require('./typeDef')
const { resolvers } = require('./resolvers')


module.exports = {
    UTBMS,
    typeDef,
    resolvers
}