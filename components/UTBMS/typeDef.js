const { gql } = require('apollo-server-express');

const typeDef = gql`
type UTBMS{
  code: String
  identifier:String
  category:String
}

input UTBMSInput{
  code: String
  identifier:String
  category:String
}


  extend type Query{
    getAllUTBMS: [UTBMS]
    findUTBMS(params:UTBMSInput): [UTBMS]
  }

`

module.exports={typeDef}