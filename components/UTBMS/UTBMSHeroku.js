/*  DATA MODEL */
const axios = require("axios");

class UTBMS {
    static getAll() {
        return axios.get('https://peaceful-taiga-59243.herokuapp.com/UTBMS')
            .then(data =>data.data)
    }
    static findUTBMS(params){
        return axios.get('https://peaceful-taiga-59243.herokuapp.com/UTBMS',{params})
        .then(data =>data.data)
    }
}

module.exports = { UTBMS }