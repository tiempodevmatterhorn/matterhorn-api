const {UTBMS} = require("./UTBMSHeroku")
const resolvers = {
    Query:  { 
        getAllUTBMS:(obj, args, context, info)=>UTBMS.getAll(),
        findUTBMS:(obj, {params}, context, info)=>{
            return UTBMS.findUTBMS({category_like:params.category})
        }
    }
}

module.exports = {
    resolvers,
  }
