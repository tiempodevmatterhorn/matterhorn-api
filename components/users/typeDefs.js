const { gql } = require("apollo-server-express")

const typeDef = gql`
type User{
    id: ID
    enrollment: Int
    lastName: String
    name: String,
    email: String
    role:String
  }

extend type Query {   
    users: [User]
    user(id:ID): User
}

`

module.exports = {typeDef};