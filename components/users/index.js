const {User} =  require("./UserHeroku")
const {resolver} =  require("./resolvers")
const {typeDef} =  require("./typeDefs")

module.exports = {
    User,resolver,
    typeDef
};
