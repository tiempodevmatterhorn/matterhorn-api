const axios = require("axios")

class User {
    static getAll() {
        return axios.get('https://peaceful-taiga-59243.herokuapp.com/users')
            .then(data => data.data)
    }

    static getById(id) {
        return axios.get('https://peaceful-taiga-59243.herokuapp.com/users', { params: { id } })
            .then(data => {
                return data.data[0]
            }
            )
    }
}

module.exports = { User };