const { User } = require('./UserHeroku')
const resolver = {
    Query: {
        users: (obj, {params}, context, info) => {
            return User.getAll()
        },
        user  : (obj, {id}, context, info) => {
            return User.getById(id)
        },
    }
}

module.exports = { resolver };