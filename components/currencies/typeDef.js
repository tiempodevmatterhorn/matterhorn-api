const { gql } = require('apollo-server-express');

const typeDef = gql`
type Currency{
    id:ID
    sign : String
    name: String
    code : String
  }

  extend type Query{
    currencies: [Currency]
  }

`

module.exports={typeDef}