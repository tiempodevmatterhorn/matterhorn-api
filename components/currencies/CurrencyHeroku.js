/*  DATA MODEL */
const axios = require("axios");

class Currency {
    static getAllCurrencies() {
        return axios.get('https://peaceful-taiga-59243.herokuapp.com/currencies')
            .then(data =>data.data)
    }
}

module.exports = { Currency }