const { Currency } = require('./CurrencyHeroku')
const { typeDef } = require('./typeDef')
const { resolvers } = require('./resolvers')


module.exports = {
    Currency,
    typeDef,
    resolvers
}