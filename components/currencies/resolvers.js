const {Currency} = require("./CurrencyHeroku")
const resolvers = {
    Query:  { 
        currencies : ()=>Currency.getAllCurrencies()
    }
}

module.exports = {
    resolvers,
  }
