const { Docket } = require('./DocketHeroku')
const { typeDef } = require('./typeDef')
const { resolvers } = require('./resolvers')


module.exports = {
    Docket,
    typeDef,
    resolvers
}