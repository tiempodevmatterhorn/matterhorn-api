const { gql } = require('apollo-server-express');

const typeDef = gql`
type Docket{
  id: ID,
  userId: ID,
  matter: Int,
  taskAction: UTBMS
  activityExpense: UTBMS
  narrative:String
  client:Client
  rate: Float,
  dueDate: Int
}

  extend type Query{
    getAllDockets: [Docket]
    getDocketByUser(userId:ID): [Docket]

  }

`

module.exports={typeDef}