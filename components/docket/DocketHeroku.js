/*  DATA MODEL */
const axios = require("axios");

class Docket {
    static getDocketsByUserId(userId) {
        return axios.get('https://peaceful-taiga-59243.herokuapp.com/dockets',{params:{userId}})
        .then(data =>data.data)
    }

    static getAllDockets() {
        return axios.get('https://peaceful-taiga-59243.herokuapp.com/dockets')
            .then(data =>data.data)
    }


}

module.exports = { Docket }