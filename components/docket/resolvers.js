const { Docket } = require("./DocketHeroku")
const resolvers = {
    Query: {
        getAllDockets: () =>Docket.getAllDockets(),
        getDocketByUser: (obj,params) => Docket.getDocketsByUserId(params.userId)
    }
}

module.exports = {
    resolvers,
}
