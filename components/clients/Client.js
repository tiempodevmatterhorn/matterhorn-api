/*  DATA MODEL */
const axios = require("axios");

class Clients {
    static getAllClients() {
        return axios.get('https://peaceful-taiga-59243.herokuapp.com/clients')
            .then(data =>data.data)
    }
}

module.exports = { Clients }