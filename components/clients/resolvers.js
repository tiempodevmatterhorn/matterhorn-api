const {Clients} = require("./Client")
const resolvers = {
    Query:  { 
        clients : ()=>Clients.getAllClients()
    }
}

module.exports = {
    resolvers,
  }
