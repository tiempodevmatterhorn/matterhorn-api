const { gql } = require('apollo-server-express');

const typeDef = gql`
type Client {
    id:ID
    name: String
    currency: Currency
  }

  extend type Query{
    clients: [Client]
  }

`

module.exports={typeDef}