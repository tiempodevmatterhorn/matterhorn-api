const { Clients } = require('./Client')
const { typeDef } = require('./typeDef')
const { resolvers } = require('./resolvers')


module.exports = {
    Clients,
    typeDef,
    resolvers
}