const UTBMS = require("./UTBMS");
const Client = require("./clients");
const Currency = require("./currencies");
const Docket = require("./docket");
const User = require("./users");

module.exports = {
    UTBMS,
    Client,
    Currency,
    Docket,
    User

}